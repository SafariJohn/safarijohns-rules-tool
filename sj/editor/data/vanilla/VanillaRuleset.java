/**
 * This file is part of SafariJohn's Rules Tool.
 * Copyright (C) 2018, 2020 SafariJohn
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package sj.editor.data.vanilla;

import java.util.*;
import sj.editor.data.Ruleset;

/**
 * Author: SafariJohn
 */
public class VanillaRuleset extends Ruleset {

    public VanillaRuleset() {
        super("Vanilla");

        //<editor-fold defaultstate="collapsed" desc="Data">
        String[] triggers = {
            "OpenInteractionDialog",
            "DialogOptionSelected",
            "PopulateOptions",
            "PrintHasSurveyDataTextIfSo",
            "PrintNearbySurveyHostilesTextIfSo",
            "MarketPostDock",
            "OutpostEstablished",
            "ReturnFromBar",
            "MarketPostOpen",
//            "FoodShortageEndedByPlayerSale",
//            "MPOAddContinue",
            "OpenCommLink",
            "BeginFleetEncounter",
            "GaveCargoToPirates",
            "TradePanelFlavorText",
            "RelationshipLevelDesc",
            "CommBurstCustomsInspection",
            "InspectionResultResponse",
            "CustomsInspectionPayOptions",
            "CustomsInspectionEndPeaceful",
            "CustomsInspectionEndHostile",
            "BeginNewGameCreation",
            "AddNewGameChoices",
            "InitNewGameChoices",
            "NGCDisableSpacerOption",
            "NewGameOptionSelected",
            "NGCSecondShipOptions",
            "NGCDifficultyOptions",
            "NGCTutorialOptions",
            "OpenCDE",
            "PickGreeting",
            "PickGreetingActive",
            "ConfirmNPCWantsToTalk",
            "NPCCommsRequest",
//            "MPMHandOverOptions",
            "PopulateOptionsHire",
            "HireOptionSet",
            "TOffScanResult",
            "CargoScanResult",
//            "PopulateOptionsISE",
//            "IGRBribeEnd",
            "SalvageCheckHostile",
            "ShowSalvageEntityDetails",
//            "DerelictPrintExtraInfo",
            "PopulateSalvageOptions1",
//            "TriggerAutomatedDefenses",
//            "BeatDefendersContinue",
            "CheckSalvageSpecial",
            "SalvageSpecialFinished",
            "SalvageSpecialFinishedNoContinue",
            "BeginSalvage",
            "PopulateSalvageOptions2",
            "CargoPodsOptions",
            "CargoPodsOptionsUpdate",
            "CargoPodsShowAvailableSupplies",
//            "ShrineMainOptions",
            "OtherPlanetInteractionText",
            "CMSNTextRejectHostile",
            "CMSNTextHasOther",
            "CMSNTextUnsuited",
            "CMSNTextSuitedPart1",
            "CMSNTextSuitedPart2",
            "CMSNTextWelcomePart1",
            "CMSNTextWelcomePart2",
            "CMSNTextChangeMind",
            "CMSNResignAskToConfirm",
            "CMSNResignConfirmed",
            "CMSNResignCancelled",
            "DisableTurnInCoresOptionIfNeeded",
            "PopulateCoresTurnInOptions",
            "AICoresTurnedIn",
//            "DistressShowAidOptions",
//            "DistressShowAvailableFuel",
//            "UpdateDstrCrewOption",
//            "DCallShowAidOptions",
//            "DCallShowAidOptionsRepeat",
//            "DCallPaymentOptions",
//            "DCallPaymentOptionsUpdate",
            "RemoveAICoreAdmin",
            "Stable_AddBuildOptions",
            "Stable_BuildConfirmOptions",
//            "COB_AddOptions",
//            "COB_DisableIndividualOptions",
//            "COB_DisableOptionsIfNeeded",
//            "COB_ConfirmOptions",
//            "COB_ConfirmPrompt",
//            "COB_SalvagingOwnedWarning",
//            "COB_ShowRepairCost",
//            "COB_ControlOwnedWarning",
//            "COB_PreActionDesc",
//            "COB_PostActionDesc",
            "BarShuttleDownPrintDesc",
            "BarPrintDesc",
//            "PrintTTLIGreeting",
//            "PopulateTTLIOptions",
//            "AdjustTTLIOptions",
//            "TTLIPayLoanResponse",
//            "TTLIExtendLoanText",
//            "TTLINotPayingText",
            "SAICPrintInitialText",
//            "PSIDisablePayOptionIfNeeded",
            "DisableRuinsExploreIfNeeded",
            "PostSalvagePerform"
        };

        getTriggers().addAll(Arrays.asList(triggers));


        String[] cFunctions = {
            "HostileFleetNearbyAndAware",
            "RepIsAtBest",
            "IsSeenByPatrols",
            "IsSoughtByPatrols",
            "RepairAvailable",
            "RepairNeeded",
            "RepairEnoughSupplies",
            "RepIsAtWorst",
//            "LPTitheCalc",
//            "DemandCargo",
            "CheckSetting",
            "NGCCanSkipTutorial",
            "NPCWantsComms",
            "PickCommsNPC",
            "HasAttentionOfAuthorities",
            "CallEvent",
            "CaresAboutTransponder",
            "Commission",
            "AICores",
            "DistressResponse",
            "Objectives",
            "FactionFleetNearbyAndAware",
            "MarketCMD",
            "DeliveryMission"
        };

        getCFunctions().addAll(Arrays.asList(cFunctions));


        String[] cVariables = {
            "$option",
            "$menuState",
            "$isHostile",
            "$isPatrol",
            "$isPerson",
            "$isStation",
            "$printedDesc",
            "$sawPlayerTransponderOff",
            "$sawPlayerWithTOffCount",
            "$ignorePlayerCommRequests",
            "$hasMarket",
            "$marketSize",
            "$stability",
            "$tradeMode",
            "$relativeStrength",
            "$talkedTo",
            "$remnantDestroyed",
            "$remnantSuppressed",
            "$remnantResurgent",
            "$damagedStation",
            "$cutCommLinkPolite",
            "$requiresDiscretionToDeal",
            "$contactedPlayerRecently",
            "$hostileToMarket",
            "$leaveGoesToMenu",
            "$entity.SEE_CONVERSATIONS_NOTES",
            "$personFaction.SEE_CONVERSATIONS_NOTES",
            "$faction.id",
            "$faction.isNeutralFaction",
            "$faction.rel",
            "$faction.friendlyToPlayer",
            "$faction.neutralToPlayer",
            "$faction.isHostile",
            "$faction.c:CUSTOM_DATA_KEY",
            "$global.isDevMode",
            "$global.tradePanelMode",
            "$global.initiatedCommsAlready",
//            "$global.customsInspectionFactionId",
//            "$global.defeatedDerelictStr",
//            "$global.tutStage",
//            "$global.ttli_unpaidEventRef",
//            "$global.ciFinished",
            "$player.transponderOn",
            "$player.credits",
            "$player.supplies",
            "$player.fuel",
            "$player.crewRoom",
            "$market.id",
            "$market.isPlanetConditionMarketOnly",
            "$market.isSurveyed",
            "$market.hasRuins",
            "$market.hasUnexploredRuins",
            "$market.ruinsExplored",
            "$market.wasCivilized",
            "$market.mc:MARKET_CONDITION_ID",
            "$market.playerHostileTimeout",
            "$market.isPlayerOwned",
            "$sourceMarket.ACCESSIBLE_FROM_SOME_FLEETS",
//    "$market.foodShortageEndedByPlayerFast",
//    "$market.foodShortageEndedByPlayer",
//    "$market.foodShortageEndedByPlayerBlack",
//    "$market.foodShortagePartiallyEndedByPlayerRemote",
//    "$market.foodShortageEndedByNPC",
//    "$market.foodShortageExpired",
            "$tag:star",
            "$tag:planet",
            "$tag:terrain",
            "$tag:system_anchor",
            "$tag:non_clickable",
            "$tag:ambient_ls",
            "$tag:has_interaction_dialog",
            "$tag:comm_relay",
            "$tag:nav_buoy",
            "$tag:sensor_array",
            "$tag:objective",
            "$tag:stable_location",
            "$tag:makeshift",
            "$tag:use_station_visual",
            "$tag:station",
            "$tag:gate",
            "$tag:orbital_junk",
            "$tag:transient",
            "$tag:jump_point",
            "$tag:stellar_mirror",
            "$tag:stellar_shade",
            "$tag:cryosleeper",
            "$tag:warning_beacon",
            "$tag:beacon_low",
            "$tag:beacon_medium",
            "$tag:beacon_high",
            "$tag:expires",
            "$tag:neutrino",
            "$tag:neutrino_low",
            "$tag:neutrino_high",
            "$tag:salvageable"
        };

        getCVariables().addAll(Arrays.asList(cVariables));


        String[] sFunctions = {
            "ShowDefaultVisual",
            "PrintDescription",
            "SetShortcut",
            "DismissDialog",
            "AddText",
            "FireBest",
            "FireAll",
            "SetEnabled",
            "OpenCoreTab",
            "MakeOptionOpenCore",
            "MarketCMD",
            "SetTooltip",
            "SetTooltipHighlightColors",
            "SetTooltipHighlights",
            "RepairAll",
            "SetTextHighlights",
            "OpenCommDirectory",
            "unsetAll",
            "EndConversation",
            "ActivateAbility",
            "UpdateMemory",
            "AddRemoveCommodity",
            "MakeOtherFleetNonHostile",
            "DemandCargo",
            "MakeOtherFleetHostile",
            "MakeOtherFleetAggressive",
            "Wait",
            "MakePlayerImmediatelyAttackable",
            "GiveOtherFleetAssignment",
            "AdjustRep",
            "CustomsInspectionGenerateResult",
            "TakeRepCheck",
            "OpenComms",
            "CustomsInspectionApplyRepLoss",
            "CustomsInspectionApplyResult",
            "unset",
            "NGCAddCredits",
            "NGCAddShip",
            "AddTextSmall",
            "NGCAddShipSilent",
            "NGCSetDifficulty",
            "NGCAddCharacterPoints",
            "NGCSetStartingLocation",
            "NGCAddStandardStartingScript",
            "NGCDone",
            "NGCAddCargo",
            "NGCSetAptitude",
            "NGCSetSkill",
            "NGCSetWithTimePass",
            "NGCAddDevStartingScript",
            "NGCSetCustom",
            "ShowPersonVisual",
            "CallEvent",
            "SetTextHighlightColors",
            "MakeHostileWhileTOff",
            "AdjustRepActivePerson",
            "CargoScan",
            "MakeOtherFleetPreventDisengage",
            "BroadcastCancelPlayerAction",
            "CargoScanApplyResult",
            "SubCredits",
            "SalvageGenFromSeed",
            "SalvageEntity",
            "PrintWreckDescription",
            "SalvageDefenderInteraction",
            "SalvageSpecialInteraction",
            "CargoPods",
            "Commission",
            "AICores",
            "MakeOtherFleetLowRepImpact",
            "DistressResponse",
            "AddAbility",
            "FleetDesc",
            "ShowPic",
            "Objectives",
            "BarCMD",
            "ShowImageVisual",
//            "DeliveryMission",
//            "RedPlanet"
        };

        getSFunctions().addAll(Arrays.asList(sFunctions));


        String[] sVariables = {
            "$option",
            "$menuState",
            "$isHostile",
            "$isPatrol",
            "$isPerson",
            "$isStation",
            "$printedDesc",
            "$sawPlayerTransponderOff",
            "$sawPlayerWithTOffCount",
            "$ignorePlayerCommRequests",
            "$hasMarket",
            "$marketSize",
            "$stability",
            "$tradeMode",
            "$relativeStrength",
            "$talkedTo",
            "$remnantDestroyed",
            "$remnantSuppressed",
            "$remnantResurgent",
            "$damagedStation",
            "$cutCommLinkPolite",
            "$requiresDiscretionToDeal",
            "$contactedPlayerRecently",
            "$hostileToMarket",
            "$leaveGoesToMenu",
            "$entity.SEE_CONVERSATIONS_NOTES",
            "$personFaction.SEE_CONVERSATIONS_NOTES",
            "$faction.id",
            "$faction.isNeutralFaction",
            "$faction.rel",
            "$faction.friendlyToPlayer",
            "$faction.neutralToPlayer",
            "$faction.isHostile",
            "$faction.c:CUSTOM_DATA_KEY",
            "$global.isDevMode",
            "$global.tradePanelMode",
            "$global.initiatedCommsAlready",
//            "$global.customsInspectionFactionId",
//            "$global.defeatedDerelictStr",
//            "$global.tutStage",
//            "$global.ttli_unpaidEventRef",
//            "$global.ciFinished",
            "$player.transponderOn",
            "$player.credits",
            "$player.supplies",
            "$player.fuel",
            "$player.crewRoom",
            "$market.id",
            "$market.isPlanetConditionMarketOnly",
            "$market.isSurveyed",
            "$market.hasRuins",
            "$market.hasUnexploredRuins",
            "$market.ruinsExplored",
            "$market.wasCivilized",
            "$market.mc:MARKET_CONDITION_ID",
            "$market.playerHostileTimeout",
            "$market.isPlayerOwned",
            "$sourceMarket.ACCESSIBLE_FROM_SOME_FLEETS",
//    "$market.foodShortageEndedByPlayerFast",
//    "$market.foodShortageEndedByPlayer",
//    "$market.foodShortageEndedByPlayerBlack",
//    "$market.foodShortagePartiallyEndedByPlayerRemote",
//    "$market.foodShortageEndedByNPC",
//    "$market.foodShortageExpired",
            "$tag:star",
            "$tag:planet",
            "$tag:terrain",
            "$tag:system_anchor",
            "$tag:non_clickable",
            "$tag:ambient_ls",
            "$tag:has_interaction_dialog",
            "$tag:comm_relay",
            "$tag:nav_buoy",
            "$tag:sensor_array",
            "$tag:objective",
            "$tag:stable_location",
            "$tag:makeshift",
            "$tag:use_station_visual",
            "$tag:station",
            "$tag:gate",
            "$tag:orbital_junk",
            "$tag:transient",
            "$tag:jump_point",
            "$tag:stellar_mirror",
            "$tag:stellar_shade",
            "$tag:cryosleeper",
            "$tag:warning_beacon",
            "$tag:beacon_low",
            "$tag:beacon_medium",
            "$tag:beacon_high",
            "$tag:expires",
            "$tag:neutrino",
            "$tag:neutrino_low",
            "$tag:neutrino_high",
            "$tag:salvageable"
        };

        getSVariables().addAll(Arrays.asList(sVariables));


        String[] tVariables = {
            "$shipOrFleet",
            "$entityName",
            "$market",
            "$onOrAt",
            "$TheFactionLong",
            "$factionIsOrAre",
            "$relAdjective",
            "$playerHostileTimeoutStr",
            "$faction",
            "$theFaction",
            "$global",
            "$theMarketFaction",
            "$PersonRank",
            "$personName",
            "$playerName",
            "$personRank",
            "$heOrShe",
            "$HeOrShe",
            "$HisOrHer",
            "$hisOrHer",
            "$entity",
            "$himOrHer",
            "$personLastName",
            "$Faction",
            "$otherFleetName",
            "$tollAmount",
            "$mpm_commodityName",
            "$fleetName",
            "$ise_bribeAmountDGS",
            "$igr_bribeAmountDGS",
            "$thePersonFaction",
            "$personFaction",
            "$nameInText",
            "$shortName",
            "$player",
            "$theOtherCommissionFaction",
            "$TheFaction",
            "$PersonPost",
            "$jangalaContactPost",
            "$jangalaContactLastName",
            "$jangalaFuel",
            "$playerFirstName",
            "$ttli_repaymentAmount",
            "$ttli_extensionDays",
            "$himOrHerself",
            "$saic_marketOnOrAt",
            "$saic_marketName",
            "$saic_heOrShe",
            "$personFirstName",
            "$psi_credits"
        };

        getTVariables().addAll(Arrays.asList(tVariables));
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="Rules">
//        DirectoryFile rootDirectory = getRootDirectory();
//
//        VanillaRulesPack1.addRules(rootDirectory);
//        VanillaRulesPack2.addRules(rootDirectory);
//        VanillaRulesPack3.addRules(rootDirectory);
//        VanillaRulesPack4.addRules(rootDirectory);
        //</editor-fold>
    }

}